DROP TRIGGER nadaj_date_realizacji on Zamowienie;
DROP FUNCTION kosztorys_suma(INT);
DROP FUNCTION zmien_status(INT,VARCHAR(25));
DROP FUNCTION kosztorys_kopiuj(INT,INT);
DROP FUNCTION data_realizacji();
DROP TABLE Uprawnienie CASCADE;
DROP TABLE Uzytkownik CASCADE;
DROP TABLE Rola CASCADE;
DROP TABLE Praca CASCADE;
DROP TABLE PozycjaKosztorysu CASCADE;
DROP TABLE Status CASCADE;
DROP TABLE Zamowienie CASCADE;
DROP TABLE Zlecenie CASCADE;



CREATE TABLE Uprawnienie (
    nazwa varchar(20) PRIMARY KEY
);

CREATE TABLE Uzytkownik (
    login varchar(20) PRIMARY KEY,
    email varchar(40) NOT NULL,
    imie varchar(20) NOT NULL,
    nazwisko varchar(20) NOT NULL,
    haslo varchar(20) NOT NULL
);

CREATE TABLE Rola (
    uzytkownik varchar(20) NOT NULL REFERENCES Uzytkownik (login),
    uprawnienie varchar(20) NOT NULL REFERENCES Uprawnienie (nazwa),
    PRIMARY KEY(uzytkownik, uprawnienie)
);

CREATE TABLE Praca (
    nazwa varchar(40) PRIMARY KEY
);

CREATE TABLE Status (
    nazwa varchar(25) NOT NULL PRIMARY KEY
);

CREATE TABLE Zamowienie (
    id serial PRIMARY KEY,
    klient varchar(20) NOT NULL REFERENCES Uzytkownik (login),
    rzeczoznawca varchar(20) REFERENCES Uzytkownik (login),
    status varchar(25) NOT NULL REFERENCES Status (nazwa) DEFAULT 'Złożone',
    tresc text NOT NULL,
    data_zlozenia timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    data_realizacji timestamp
);

CREATE TABLE PozycjaKosztorysu (
    praca varchar(40) NOT NULL REFERENCES Praca (nazwa),
    zamowienie int NOT NULL REFERENCES Zamowienie (id),
    koszt numeric(10,2) NOT NULL DEFAULT 0.00,
    zrealizowane boolean NOT NULL DEFAULT true,
    CONSTRAINT koszt_positive check (koszt >= 0),
    PRIMARY KEY (praca, zamowienie)
);

CREATE TABLE Zlecenie (
    id serial PRIMARY KEY,
    klient varchar(20)  NOT NULL REFERENCES Uzytkownik (login),
    zaakceptowane boolean  NOT NULL DEFAULT true,
    kierownik varchar(20)  NOT NULL REFERENCES Uzytkownik (login),
    zamowienie int NOT NULL REFERENCES Zamowienie (id)
);


CREATE FUNCTION kosztorys_suma(INT) RETURNS NUMERIC(10,2) AS $$
    SELECT sum(koszt) FROM PozycjaKosztorysu WHERE zamowienie=$1;
$$ LANGUAGE SQL;

CREATE FUNCTION zmien_status(INT, VARCHAR(25)) RETURNS VOID AS $$
    UPDATE Zamowienie
    SET Status=$2
    WHERE id=$1;
$$ LANGUAGE SQL;

CREATE FUNCTION kosztorys_kopiuj (INT, INT) RETURNS VOID AS $$
    UPDATE Zamowienie
    SET rzeczoznawca=(SELECT rzeczoznawca FROM Zamowienie WHERE id=$1)
    WHERE id=$2;

    INSERT INTO PozycjaKosztorysu (praca,zamowienie,koszt,zrealizowane)
    SELECT praca,$2,koszt,true as zrealizowane
    FROM PozycjaKosztorysu
    WHERE zamowienie=$1;
$$ LANGUAGE SQL;

CREATE FUNCTION data_realizacji() RETURNS TRIGGER AS $$
BEGIN
    IF NEW.status = 'Przekazane do realizacji' THEN
        NEW.data_realizacji=CURRENT_TIMESTAMP;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER nadaj_date_realizacji
    BEFORE UPDATE ON Zamowienie
    FOR EACH ROW
    EXECUTE PROCEDURE data_realizacji();


--values
INSERT INTO Uzytkownik
VALUES
    ('andpe', 'andpe@gmail.com', 'Andrzej', 'Pęcina', 'andpe'),
    ('przemokraw', 'przemekkraw@gmail.com', 'Przemysław', 'Krawczyk', 'haslo'),
    ('miehot', 'miehot@gmail.com', 'Mieczysław', 'Hotkowski', 'miehot'),
    ('krzywy', 'krzywy@gmail.com', 'Krzysztof', 'Wyga', 'krzywy'),
    ('jadbar', 'jadbar@gmal.com', 'Jadwiga', 'Barańska', 'jadbar'),
    ('albud', 'albud@gmail.com', 'Alicja', 'Budka', 'albud'),
    ('tomtaj', 'tomtaj@gmail.com', 'Tomislaw', 'Tajner', 'tomtaj'),
    ('admal', 'admal@gmail.com', 'Adam', 'Małysz', 'admal'),
    ('serwil', 'serwil@gmail.com', 'Serena', 'Williams', 'serwil');

INSERT INTO Uprawnienie
VALUES
    ('Klient'),
    ('Rzeczoznawca'),
    ('Inspektor'),
    ('Kierownik robót');

INSERT INTO Rola
VALUES
    ('andpe', 'Klient'),
    ('przemokraw', 'Klient'),
    ('przemokraw', 'Rzeczoznawca'),
    ('przemokraw', 'Inspektor'),
    ('miehot', 'Klient'),
    ('krzywy', 'Rzeczoznawca'),
    ('jadbar', 'Klient'),
    ('jadbar', 'Rzeczoznawca'),
    ('albud', 'Klient'),
    ('tomtaj', 'Inspektor'),
    ('admal', 'Rzeczoznawca'),
    ('serwil', 'Kierownik robót');

INSERT INTO Status
VALUES
    ('Złożone'),
    ('W trakcie'),
    ('Oczekuje na klienta'),
    ('Przekazane do realizacji'),
    ('Zrealizowane');

INSERT INTO Praca
VALUES
    ('Strzyżenie trawnika'),
    ('Brukowany chodnik'),
    ('Postawienie płotu');
