#!env/bin/python
# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras
from flask import Flask, g, render_template, request, redirect, url_for, session, flash
from difflib import SequenceMatcher

# configuration
DEBUG = True
SECRET_KEY = 'key'

app = Flask(__name__)
app.config.from_object(__name__)

def similar(a,b):
    return SequenceMatcher(None,a,b).ratio()

def connect_db():
#    con = psycopg2.connect("dbname='bd' user='pk305178' host='labdb' password='Przemo8'")
    con = psycopg2.connect("", cursor_factory=psycopg2.extras.DictCursor)
    con.autocommit = True
    con.set_client_encoding("utf8")
    return con

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_db()
    return db

def get_cursor():
    return get_db().cursor()

def has_permission(login, permission):
    cur = get_cursor()
    cur.execute("SELECT uprawnienie FROM Rola WHERE uzytkownik='{0}'".format(login))
    return permission in [p[0] for p in cur.fetchall()]

# views
@app.route('/static/<filename>')
def send_static(filename):
    return app.send_static_file(filename)

@app.route('/home')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    next_page = 'index'
    if request.method == 'POST':
        login = request.form['login']
        password = request.form['password']
        next_page = request.form['next']

        cur = get_cursor()
        cur.execute("SELECT * from Uzytkownik where login='{0}'\n".format(login))
        row = cur.fetchone()
        if not row:
            flash("Brak użytkownika o loginie {0}.".format(request.form['login']), 'error')
        elif row[4] == request.form['password']:
            session['login'] = request.form['login']
            session['email'] = row[1]
            session['name'] = row[2]
            session['surname'] = row[3]
            flash("Zalogowano pomyślnie", 'success')
        else:
            flash("Błędne hasło", 'warning')
    return redirect(url_for(next_page))


@app.route('/logout')
def logout():
    session.pop('login', None)
    session.pop('email', None)
    session.pop('name', None)
    session.pop('surname', None)
    flash("Wylogowano pomyślnie", 'success')
    return redirect(url_for('index'))




@app.route('/order')
def order():
    active=True
    if 'login' in session and not has_permission(session['login'], 'Klient'):
        flash("Aby złożyć zamówienie, musisz posiadać konto klienta", "warning")
        active=False
    return render_template("order.html", active=active)


@app.route('/my_orders')
def my_orders():
    rows = None
    if 'login' in session:
        cur = get_cursor()
        cur.execute(
                "SELECT id, to_char(data_zlozenia, 'DD.MM.YYYY HH24:MI:SS') as data, tresc, status " + 
                "FROM Zamowienie " +
                "WHERE klient='{0}'".format(session['login'])
                )
        rows = cur.fetchall()
    return render_template('my_orders.html', rows=rows)


@app.route('/add_order', methods=['GET', 'POST'])
def add_order():
    if request.method == 'POST':
        login = session['login']
        order = request.form['order']
        if not order:
            flash("Wypełnij treść zamówienia.", "error")
            return redirect(url_for('order'))

        cur = get_cursor()
        cur.execute(
            "INSERT INTO Zamowienie (klient, tresc) " +
            "VALUES ('{0}', '{1}')".format(login,order)
            )
    return redirect(url_for('my_orders'))


@app.route('/remove_order', methods=['GET', 'POST'])
def remove_order():
    if request.method == 'POST':
        order = request.form['order']

        cur = get_cursor()
        cur.execute(
            "DELETE FROM Zamowienie * " +
            "WHERE id={0}".format(order)
            )
        flash("Usunięto zamówienie nr {0}".format(order), "success")
    return redirect(url_for('my_orders'))


@app.route('/order_<order_id>')
def order_detail(order_id):
    try:
        int(order_id)
    except:
        flash("Niepoprawne id", "error")
        return redirect(url_for('my_orders'))

    rows = None
    if 'login' in session and has_permission(session['login'], 'Klient'):
        cur = get_cursor()
        cur.execute(
                "SELECT * " 
                "FROM PozycjaKosztorysu " +
                "WHERE zamowienie={0} ".format(order_id)
                )
                
        rows = cur.fetchall()
    return render_template('order_detail.html', rows=rows)


@app.route('/add_user', methods=['GET', 'POST'])
def add_user():
    if request.method == 'POST':
        login = session['login']

        cur = get_cursor()
        cur.execute(
            "INSERT INTO Rola " +
            "VALUES ('{0}', '{1}')".format(login,'Klient')
            )
        flash("Pomyślnie utworzono konto klienta. Możesz teraz dokonywać zakupów.", "success")
    return redirect(url_for('order'))


@app.route('/update_order', methods=['GET','POST'])
def update_order():
    if request.method == 'POST':
        works = request.form.getlist('selected')
        if works:
            works = str(tuple(works)) if len(works) > 1 else "('" + works[0] + "')"
        order = request.form.get("order")
        cur = get_cursor()
        cur.execute(
                "UPDATE PozycjaKosztorysu " +
                "SET zrealizowane=false " +
                "WHERE zamowienie={0}".format(order)
                )

        if works:
            cur.execute(
                    "UPDATE PozycjaKosztorysu " +
                    "SET zrealizowane=true " +
                    "WHERE zamowienie={0} and praca in {1}".format(order, works)
                    )
        flash("Zapisano zmiany", "success")
    return redirect(url_for('my_orders'))


@app.route('/expert_panel_order_<order_id>')
def expert_panel_order(order_id):
    order = None
    try:
        int(order_id)
    except:
        flash("Niepoprawne id", "error")
        return redirect(url_for('expert_panel'))

    if not 'login' in session:
        return render_template('expert_panel_orders.html')

    login = session['login']
    if not has_permission(login, 'Rzeczoznawca'):
        flash('Nie masz uprawnień do oglądania tej strony')
    else:
        cur = get_cursor()
        # wybieram zamówienie
        cur.execute(
                "SELECT id, klient, to_char(data_zlozenia, 'DD.MM.YYYY HH24:MI:SS') as data, tresc, rzeczoznawca " +
                "FROM Zamowienie " +
                "WHERE id='{0}'".format(order_id)
                )
        order = cur.fetchone()
        # wybieram obecne prace
        cur.execute(
                "SELECT * FROM PozycjaKosztorysu " +
                "WHERE zamowienie={0}".format(order_id))
        current_works = cur.fetchall()

        # wybieram dostępne prace
        cur.execute("SELECT * FROM Praca")
        available_works = cur.fetchall()
    return render_template('expert_panel_orders.html', order=order, available_works=available_works, current_works=current_works)


@app.route('/expert_panel')
def expert_panel():
    rows = None
    if 'login' in session:
        login = session['login']
        if not has_permission(session['login'], 'Rzeczoznawca'):
            flash('Nie masz uprawnień do wyświetlania tej strony', 'error')
        else:
            cur = get_cursor()
            cur.execute("SELECT id, klient, to_char(data_zlozenia, 'DD.MM.YYYY HH24:MI:SS') as data, tresc " + 
                        "FROM Zamowienie " +
                        "WHERE rzeczoznawca='{0}' and status='W trakcie'".format(login))
            rows = cur.fetchall()
            if not rows:
                flash("Brak nieobsłużonych zamówień", "warning")
    return render_template('expert_panel.html', rows=rows)


@app.route('/delete_work', methods=['GET','POST'])
def delete_work():
    order_id = None
    if request.method == 'POST':
        work = request.form['work']
        order_id = request.form['id']
        cur = get_cursor()
        cur.execute("DELETE FROM PozycjaKosztorysu " +
                    "* WHERE zamowienie={0} and praca='{1}'".format(order_id,work)
                    )
        flash("Usunięto pozycję {0} z kosztorysu".format(work), "info")
    return redirect(url_for('expert_panel_order', order_id=order_id))


@app.route('/insert_work', methods=['GET','POST'])
def insert_work():
    order_id = None
    if request.method == 'POST':
        # dodawanie nowej pracy
        work = request.form.get('work', None)
        price = request.form.get('koszt', None)
        order_id = request.form.get('id', None)
        try:
            cur = get_cursor()
            cur.execute("INSERT INTO PozycjaKosztorysu " +
                        "VALUES ('{0}',{1},{2})".format(work,order_id,price))
            flash("Dodano pozycję {0} do kosztorysu".format(work), "success")
        except psycopg2.IntegrityError:
            flash("Wybrana praca jest już w kosztorysie", "error")
    return redirect(url_for('expert_panel_order', order_id=order_id))


@app.route('/add_work', methods=['GET','POST'])
def add_work():
    order_id = None
    if request.method == 'POST':
        # dodawanie nowej pracy
        work = request.form.get('work', None)
        order_id = request.form.get('id', None)
        try:
            cur = get_cursor()
            cur.execute("INSERT INTO Praca " +
                        "VALUES ('{0}')".format(work))
            flash("Pomyślnie dodano nowy rodzaj pracy: {0}".format(work), "success")
        except psycopg2.IntegrityError:
            flash("Podana praca już istnieje w bazie", "error")
    return redirect(url_for('expert_panel_order', order_id=order_id))

@app.route('/update_status', methods=['GET','POST'])
def update_status():
    if request.method == 'POST':
        # dodawanie nowej pracy
        order_id = request.form.get('id', None)
        status = request.form.get('status', None)
        next_url = request.form.get('next', None)
        message = request.form.get('message', None)
        # try:
        cur = get_cursor()
        cur.execute("SELECT zmien_status({0},'{1}')".format(order_id, status))
        flash(message, "success")
        #except:
        #    flash("Błąd bazy danych", "error")
    return redirect(url_for(next_url))


@app.route('/inspector_panel_order_<order_id>', methods=['GET','POST'])
def inspector_panel_order(order_id):
    order = None
    experts = None
    try:
        int(order_id)
    except:
        flash("Niepoprawne id", "error")
        return redirect(url_for('inspector_panel'))

    if not 'login' in session:
        return render_template('inspector_panel_orders.html')

    login = session['login']
    if not has_permission(login, 'Inspektor'):
        flash('Nie masz uprawnień do oglądania tej strony')
    else:
        # wybieram zamównienie
        cur = get_cursor()
        cur.execute(
                "SELECT id, klient, to_char(data_zlozenia, 'DD.MM.YYYY HH24:MI:SS') as data, tresc, rzeczoznawca " +
                "FROM Zamowienie WHERE id='{0}'".format(order_id)
                )
        order = cur.fetchone()

        # wybieram rzeczoznawców
        cur.execute(
            "SELECT * FROM Uzytkownik WHERE login IN " + 
            "(SELECT uzytkownik FROM Rola where uprawnienie='Rzeczoznawca')"
            )
        experts = cur.fetchall()

        # szukam podobnych zamówień
        cur.execute("SELECT id,tresc FROM Zamowienie WHERE status='Przekazane do realizacji'") #TODO: where data_realizacji is non null
        similar_orders = cur.fetchall()
        similar_orders = {sim['id']: similar(order['tresc'],sim['tresc']) for sim in similar_orders}
        similar_orders = sorted(similar_orders, key=lambda x:similar_orders[x])[-3:]

        estimates = []
        for i in range(len(similar_orders)):
            cur.execute("SELECT praca FROM PozycjaKosztorysu WHERE zamowienie={0}".format(similar_orders[i]))
            estimates.append({"id": similar_orders[i], "works": [work['praca'] for work in cur.fetchall()]})


    return render_template('inspector_panel_orders.html', order=order, experts=experts, estimates=estimates)

@app.route('/update_expert', methods=['GET', 'POST'])
def update_expert():
    if request.method == 'POST':
        expert = request.form['expert']
        order_id = request.form['id']
        cur = get_cursor()
        cur.execute(
                "UPDATE Zamowienie " +
                "SET rzeczoznawca='{0}' ".format(expert) + 
                "WHERE id='{0}'".format(order_id)
                )
        flash("Pomyślnie przydzielono rzeczoznawcę do zamówienia nr {0}. Możesz teraz wysłać zamówienie do rzeczoznawcy w celu sporządzenia kosztorysu lub zmienić przydzielonego rzeczoznawcę".format(order_id), "success")
    return redirect(url_for("inspector_panel"))




@app.route('/copy_order', methods=['GET', 'POST'])
def copy_order():
    if request.method == 'POST':
        old = request.form.get('old_order_id')
        new = request.form.get('new_order_id')
        cur = get_cursor()
        cur.execute("SELECT kosztorys_kopiuj({0},{1})".format(old,new))
        cur.execute("SELECT zmien_status({0},'{1}')".format(new,"Oczekuje na klienta"))
        flash("Skopiowano kosztorys. Został on przekazany do klienta w celu weryfikacji", "success")
    return redirect(url_for('inspector_panel'))

@app.route('/inspector_panel')
def inspector_panel():
    new_orders = None
    exp_orders = None
    cur_orders = None
    old_orders = None
    if 'login' in session:
        login = session['login']
        if not has_permission(login, 'Inspektor'):
            flash('Nie masz uprawnień do wyświetlania tej strony', 'error')
        else:
            # wybieram zamówienia wymaghające obsłużenia
            cur = get_cursor()
            cur.execute(
                "SELECT id, klient, to_char(data_zlozenia, 'DD.MM.YYYY HH24:MI:SS') as data, tresc, rzeczoznawca " +
                "FROM Zamowienie WHERE status='Złożone'"
                )
            new_orders = cur.fetchall()
            if not new_orders:
                flash("Brak nieobsłużonych zamówień", "warning")

            # wybieram zamówienia oczekujące na utworzenie kosztorysu
            cur.execute(
                "SELECT id, klient, to_char(data_zlozenia, 'DD.MM.YYYY HH24:MI:SS') as data, tresc, rzeczoznawca " +
                "FROM Zamowienie WHERE status='W trakcie'"
                )
            exp_orders = cur.fetchall()

            # wybieram zamówienia oczekujące na akceptację
            cur.execute(
                "SELECT id, klient, to_char(data_zlozenia, 'DD.MM.YYYY HH24:MI:SS') as data, tresc, rzeczoznawca " +
                "FROM Zamowienie WHERE status='Oczekuje na klienta'"
                )
            cur_orders = cur.fetchall()

            # wybieram zamówienia czekające na realizację
            cur.execute(
                "SELECT id, klient, to_char(data_zlozenia, 'DD.MM.YYYY HH24:MI:SS') as data_zlozenia, "+
                "to_char(data_realizacji, 'DD.MM.YYYY HH24:MI:SS') as data_realizacji, tresc, rzeczoznawca " +
                "FROM Zamowienie WHERE status='Przekazane do realizacji'"
                )
            old_orders = cur.fetchall()



    return render_template('inspector_panel.html', new_orders=new_orders, exp_orders=exp_orders, cur_orders=cur_orders, old_orders=old_orders)


if __name__ == '__main__':
    app.run()
